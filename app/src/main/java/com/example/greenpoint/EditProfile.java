package com.example.greenpoint;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class EditProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
    }

    public void SaveProfile(View view) {
        Intent intent = new Intent(getApplicationContext(), com.example.greenpoint.Profile.class);
        startActivity(intent);
    }
}