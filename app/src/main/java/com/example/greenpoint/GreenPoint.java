package com.example.greenpoint;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface GreenPoint {

    public static final String BASE_URL = "https://itlearningcenters.com/android/project0206/";

    @FormUrlEncoded
    @POST("regist_cust.php")
    Call<ResponseBody> registerUser(@Field("cus_name") String userNameValue,
                                    @Field("email") String emailValue,
                                    @Field("username") String usernameValue,
                                    @Field("phone") String phoneValue,
                                    @Field("password") String passwordValue);

//    Call<ResponseBody> registerUser(String usernameValue, String passwordValue, String emailValue, String phoneValue);
}
