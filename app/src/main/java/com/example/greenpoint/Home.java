package com.example.greenpoint;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void Profile(View view) {
        Intent intent = new Intent(getApplicationContext(), com.example.greenpoint.Profile.class);
        startActivity(intent);
    }

    public void SearchShop(View view) {
        Intent intent = new Intent(getApplicationContext(), com.example.greenpoint.SearchShop.class);
        startActivity(intent);
    }

    public void QRcode(View view) {
        Intent intent = new Intent(getApplicationContext(), com.example.greenpoint.QRcode.class);
        startActivity(intent);
    }

    public void ExchangeReward(View view) {
        Intent intent = new Intent(getApplicationContext(), com.example.greenpoint.ExchangeReward.class);
        startActivity(intent);
    }

    public void MyPoint(View view) {
        Intent intent = new Intent(getApplicationContext(), com.example.greenpoint.MyPoint.class);
        startActivity(intent);
    }

    public void Logout(View view) {
        Intent intent = new Intent(getApplicationContext(), com.example.greenpoint.Login.class);
        startActivity(intent);
    }
}