package com.example.greenpoint;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class QRcode extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
    }

    public void Finish(View view) {
        Intent intent = new Intent(getApplicationContext(),Home.class);
        startActivity(intent);
    }
}